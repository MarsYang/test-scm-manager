# docker-scm-manager #

Docker container for [SCM-Manager](http://www.scm-manager.org).

## usage ##


```
#!bash

mkdir /var/lib/scm
chown 1000:1000 /var/lib/scm
docker run -v /var/lib/scm:/var/lib/scm -p 8080:8080 sdorra/scm-manager
```